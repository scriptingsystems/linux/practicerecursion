**Glob** patterns specify sets of filenames with wildcard characters
[https://cmdchallenge.com/#/print_line_before](CMD Challenge)

Buscar en subdirectorios
```sh
$ ll **/*.doc
```

```sh
$ for myfile in **/*.* ;do echo $myfile ;done
ansible-automation-platform-setup-bundle-2.3-1.2/README.md
ansible-automation-platform-setup-bundle-2.3-1.2/setup.sh
PROJECT/script1.sh
PROJECT/script2.sh

$ for myfile in **/*.* ; do mv ${myfile} ${myfile%.*} ; done
```


